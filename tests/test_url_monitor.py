from configparser import ConfigParser
from monitoring_tool.url_monitor import monitor_urls


config = {
        'webpages': {
            'url_1': 'https://www.example.com',
            'content_1': 'Example Domain',
            'url_2': 'https://www.example.com',
            'content_2': 'fake Content',
            'url_3': 'http://non-existent-url.com',
            'content_3': 'Example Domain',
        }
    }

parser = ConfigParser()
parser.read_dict(config)


def test_monitor_urls_successful():
    url = parser.get('webpages', 'url_1')
    content = parser.get('webpages', 'content_1')

    assert monitor_urls(url, content) == True


def test_monitor_urls_with_value_error():
    url = parser.get('webpages', 'url_2')
    content = parser.get('webpages', 'content_2')

    assert monitor_urls(url, content) == False


def test_monitor_urls_with_connection_error():
    url = parser.get('webpages', 'url_3')
    content = parser.get('webpages', 'content_3')

    assert monitor_urls(url, content) == False
