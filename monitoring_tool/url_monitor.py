import requests
import time
import logging
from datetime import datetime
from configparser import ConfigParser


# Set the maximum allowed response time (in seconds)
max_response_time = 10

# Set the time interval for checking the URLs (in seconds)
check_interval = 5

# Set up the logging configuration
log_file_name = f"requests_{datetime.now().strftime('%Y-%m-%d_%H-%M-%S')}.log"
logging.basicConfig(filename=log_file_name, level=logging.INFO, format='%(asctime)s: %(levelname)s - %(message)s')

# Read URLs, and content requirements for each URL from the configuration file

file = "resources/config.ini"
config = ConfigParser()
config.read(file)

# Define a function to monitor the URLs


def monitor_urls(url, content):

    try:

        start_time = time.time()
        response = requests.get(url, timeout=max_response_time)
        response_time = time.time() - start_time

        if response.status_code == 200:

            if content not in response.text:
                failure_message = f"{url} is not available. Content requirement not found in the response"
                logging.warning(failure_message)
                return False

            success_message = f"{url} is available (response time: {response_time:.2f} seconds)"
            logging.info(success_message)
            return True

        else:
            print(f"{url} is not Available. Status code:", response.status_code)

    except Exception as e:

        failure_message = f"{url} Request results in Exception ({e})"
        logging.warning(failure_message)
        return False


# Run the program continuously

if __name__ == '__main__':
    print("..... Monitoring Availability .....")

    while True:
        try:
            for key, url in config['webpages'].items():
                if key.startswith('url_'):
                    content_key = 'content_' + key.split('_')[1]
                    content = config['webpages'][content_key]
                    monitor_urls(url, content)
            time.sleep(check_interval)

        except KeyboardInterrupt:
            logging.info('Exiting gracefully...')
            break
