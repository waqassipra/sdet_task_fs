# Website Monitoring Task Solution

## Overview
This project is a solution for a website monitoring task. The aim of the project is to read a list of URLs and their corresponding content requirements from a configuration file, and then monitor those URLs.

## Main Tech stacks

- [Pytest](https://docs.pytest.org/) Python testing framework
- [Python-requests](https://requests.readthedocs.io/en/latest/) Python requests library


## Project Structure
The project is manly consists of following three:
 * [Project](./)
   * [monitoring_tool](./monitoring_tool) Contains task solution
   * [tests](./tests) Contains test files
   * [resources](./resources) Contains common resources (configuration file)

## Pre-requisites
- Python
- Pytest
- Requests Library

## How to run tests
- Install all dependencies using following command under [requirements](./requirements.txt)
```
pip3 install -r requirements.txt
```
- Execute the task solution by running the following command in the terminal from the project's root directory:
```
python monitoring_tool/url_monitor.py
```
- Execute the test cases by running the following command in the terminal from the project's root directory:
```
pytest tests/test_url_monitor.py
```
## Test Results

Test execution would result in generating a .log file containing test execution results.

- Here's a sample of what the log file might look like:
```
2023-03-20 02:12:41,614: INFO - https://www.example.com is available (response time: 0.53 seconds)
2023-03-20 02:12:41,863: INFO - https://www.python.org is available (response time: 0.25 seconds)
2023-03-20 02:12:42,526: WARNING - https://www.github.com is not available. Content requirement not found in the response
2023-03-20 02:12:42,992: INFO - https://www.nytimes.com is available (response time: 0.46 seconds)
2023-03-20 02:12:43,327: INFO - https://www.wikipedia.org is available (response time: 0.33 seconds)
```